//EXPLICACION DE CADA LINEA DE CODIGO 
import java.util.HashMap;// PRIMERO REALICE UN import de clase "HasMap" para almacenar datos de inventario
import java.util.Map;// SEGUNDO REALICE UN INTERFAZ "Map" del paquete 'java.util' que utilizare para las asociacioens producto_cantidades
import java.util.Scanner;//TERCERO UN Scanner que lo utilizare para leer cada entrada de consola

public class Tornillo_Feliz {//declaro la clase "Tonirllo_Feliz"

    private static Scanner scanner = new Scanner(System.in); //como anteriormente lo dije creo un objeto Scanner para leer la entrada de usuario
    private static Map<String, Integer> inventarioHistorico = new HashMap<>();
    private static Map<String, Integer> ventasHistoricas = new HashMap<>();
    private static Map<String, Integer> demandaFutura = new HashMap<>();
    private static Map<String, String> usuariosRegistrados = new HashMap<>();
    private static String usuarioActual = null;

    public static void main(String[] args) {  //defino el metodo main como punto de inicio
        inicializarDatos();
        inicioSesionORegistro();
//Inicia un bucle while infinito para mantener el programa en funcionamiento hasta que se cierre explícitamente.
        while (true) {
            System.out.println("\n¿Qué acción deseas realizar?");
            System.out.println("1. Comprar un producto");
            System.out.println("2. Buscar un producto");
            System.out.println("3. Ver inventario");
            System.out.println("4. Cerrar sesión");
            System.out.print("Ingrese el número correspondiente a la acción: ");
            int opcion = scanner.nextInt();
            scanner.nextLine(); // Consumir el salto de línea // MOSTRAMOS LAS DISTINTAS OPCIONES DE MENU PARA QUE EL USUARIO ELIJA
// realizo un Switch para ejecutar el metodo correspodiente
            switch (opcion) {
                case 1:
                    comprarProducto();
                    break;
                case 2:
                    buscarProducto();
                    break;
                case 3:
                    verInventario();
                    break;
                case 4:
                    cerrarSesion();
                    break;
                default:
                    System.out.println("Opción no válida. Por favor, ingresa un número válido.");
            }
        }
    }
// Simulación de datos de inventario histórico
    private static void inicializarDatos() {
        inventarioHistorico.put("Tornillos", 100);
        inventarioHistorico.put("Clavos", 150);
        inventarioHistorico.put("Tuercas", 80);

        ventasHistoricas.put("Tornillos", 80);
        ventasHistoricas.put("Clavos", 120);
        ventasHistoricas.put("Tuercas", 70);

        for (String producto : inventarioHistorico.keySet()) {
            int demandaPromedio = (inventarioHistorico.get(producto) + ventasHistoricas.get(producto)) / 2;
            demandaFutura.put(producto, demandaPromedio);
        }
//metodo usuario para registro y contraseña
        usuariosRegistrados.put("usuario1", "contraseña1");
        usuariosRegistrados.put("usuario2", "contraseña2");
    }

    private static void inicioSesionORegistro() {
        while (true) {
            System.out.println("\n¿Qué acción deseas realizar?");
            System.out.println("1. Iniciar sesión");
            System.out.println("2. Registrarse como nuevo usuario");
            System.out.print("Ingrese el número correspondiente a la acción: ");
            int opcion = scanner.nextInt();
            scanner.nextLine(); // Consumir el salto de línea

            switch (opcion) {
                case 1:
                    iniciarSesion();
                    return;
                case 2:
                    registrarUsuario();
                    return;
                default:
                    System.out.println("Opción no válida. Por favor, ingresa un número válido.");
            }
        }
    }

    private static void iniciarSesion() {
        System.out.print("Ingrese su nombre de usuario: ");
        String usuario = scanner.nextLine();
        System.out.print("Ingrese su contraseña: ");
        String contraseña = scanner.nextLine();

        if (usuariosRegistrados.containsKey(usuario) && usuariosRegistrados.get(usuario).equals(contraseña)) {
            usuarioActual = usuario;
            System.out.println("Inicio de sesión exitoso. Bienvenido, " + usuarioActual + "!");
        } else {
            System.out.println("Nombre de usuario o contraseña incorrectos. Por favor, inténtalo nuevamente.");
            inicioSesionORegistro();
        }
    }

    private static void registrarUsuario() {
        System.out.print("Ingrese un nombre de usuario: ");
        String nuevoUsuario = scanner.nextLine();
        if (usuariosRegistrados.containsKey(nuevoUsuario)) {
            System.out.println("El nombre de usuario ya está en uso. Por favor, elige otro.");
            registrarUsuario();
        } else {
            System.out.print("Ingrese una contraseña: ");
            String nuevaContraseña = scanner.nextLine();
            usuariosRegistrados.put(nuevoUsuario, nuevaContraseña);
            System.out.println("Usuario registrado exitosamente. Ahora puedes iniciar sesión.");
            inicioSesionORegistro();
        }
    }

    private static void comprarProducto() {
        if (usuarioActual == null) {
            System.out.println("Debes iniciar sesión para comprar productos.");
            return;
        }
        // Lógica para comprar productos
        System.out.println("Funcionalidad de compra de productos en construcción.");
    }

    private static void buscarProducto() {
        System.out.println("Funcionalidad de búsqueda de productos en construcción.");
    }

    private static void verInventario() {
        System.out.println("Funcionalidad de ver inventario en construcción.");
    }

    private static void cerrarSesion() {
        usuarioActual = null;
        System.out.println("Sesión cerrada exitosamente. Hasta pronto!");
        inicioSesionORegistro();
    }
}



